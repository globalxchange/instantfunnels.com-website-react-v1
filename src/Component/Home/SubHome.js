import React, { Component } from 'react'

import { ProductConsumer } from "../../ContextApi";
import { NavLink, Redirect } from "react-router-dom";
import {Button,Modal,Form,DropdownButton,Dropdown} from 'react-bootstrap'
// import YouTube from 'react-youtube';
import ReactPlayer from 'react-player'
import axios from "axios";
import { SketchPicker } from 'react-color';
import Insta from '../logo/insta.png'
import {
ClickAwayListener} from '@material-ui/core';
import Duration from '../Duration'
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import {Fontweight} from './whatgxapi'
import Draggable from 'react-draggable';
let lol=""
const array1 = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,50,51,52,53,54,55,56,57,58,59,60];
let destopheadertitle={}
let destopheadertitleDIv={}
let destopheadertitlepar={}
let destopkbutton={}
let destopheadertitleDIvpar={}
let destopkbuttonActiveNavbar={}
let destopkbuttonActive={}
export default class SubHome extends Component {

 static contextType = ProductConsumer;
 
state={
    pip: false,
    // playing: true,
     contentState: JSON.parse('{"entityMap":{},"blocks":[{"key":"1ljs","text":"Turn Your Trades Into","type":"unstyled","depth":0,"inlineStyleRanges":[],"entityRanges":[],"data":{}}]}'),
    controls: true,
    activeDrags: 0,
    light: false,
    volume: 0.8,
    contentEditext:JSON.parse('{"entityMap":{},"blocks":[{"key":"1ljs","text":"Turn Your Trades Into","type":"unstyled","depth":0,"inlineStyleRanges":[],"entityRanges":[],"data":{}}]}'),
    muted: false,
    loaded: 0,
    duration: 0,
    showtogglesetting:false,//as
    playbackRate: 1.0,
    loop: false,
    showmediumsetting:true,
    showmediumsettingshow:false,
    showtogglesettingpar:false,
    showmediumsettingpar:false,
    active:false,
    opencolorsettingshowbutton:false,
    showntbuttonactive:false,
    showclassactive:false,
    DesktopHeaderbackgroundshowbutton:"transpartant",
    TextDesktopHeaderbackgroundshowbutton:"white",
    shownxtbutton:false,
    windowWidth:"",
    mobilewidth:"100%",
    headertitleinput:"Header Text",
    headertitleinputpar:"GXLive - The #1 Application Marketplace For Cryptocurrency Integrated Applications. GXLive - The #1 Application Marketplace For Cryptocurrency Integrated Applications. GXLive.",
    Desktopfontsizeheaderpar:18,
    DesktopfontsizeheaderButton:18,
    Desktopfontsizeheader:60,
    Desktopheadfontweight:"bold",
    DesktopheadfontweightName:"bold",
    Desktopheadfontweightpar:"Normal",
    Desktopheadfontweightbutton:"Normal",
    DesktopheadfontweightNamebutton:"Normal",
    DesktopheadfontweightNamepar:"Normal",
    DesktopHeaderbackground:"white",
    DesktopHeaderbackgroundpar:"white",
    DesktopHeaderborderbuttonbackgrond:"black",
    opencolorsetting:false,
    opencolorsettingpar:false,
    showmediumsettingshowpar:false,
    showsickerpickercolor:false,
    DHpaddingLeft:0,
    DHpaddingRight:0,
    DHpaddingTop:0,
    DHpaddingBottom:0,
    DesktopAlignText:"left",
    DesktopPAlignTextpar:"left",
    DesktopPAlignTextnext:"center",
    ButtonTextnext:"Next Step",
    ButtonTextnextnoActive:"Next Step",
    buttonnamenoactive:"Next Step",
    Textopencolorsettingshowbutton:false,
    DHMLeft:0,
    DHMRight:0,
    DHMTop:0,
    DHMBottom:0,
    DHPLeft:0,
    DHPRight:0,
    DHPTop:0,
    DHPBottom:0,
    DHMpaddingLeft:0,
    nextbuttonwidth:90,
    DHMpaddingRight:0,
    DHMpaddingTop:0,
    DHMpaddingBottom:0,
    nextbuttonheight:31,
    showbuttonsettingrich:true,
    imagePreviewUrl:Insta,
    
    selectedFile:"https://toshimarkets.nyc3.cdn.digitaloceanspaces.com/Video%20On%20Setupmywallet.com.mp4",
    DNextpaddingLeft:0,
    DNextpaddingRight:0,
    DNextpaddingTop:0,
    DNextpaddingBottom:0,
    DesktopHeaderborderbutton:"white",
    nextbuttonborder:1,
    DButtonLeft:0,
    DButtonRight:0,
    DButtonTop:0,
    DButtonBottom:0,

    // 
    DesktopheadfontweightNamebuttonActive:"Normal",
    DesktopheadfontweightbuttonActive:"Normal",
    nextbuttonwidthActive:90,
    nextbuttonheightActive:10,
    nextbuttonborderAcitve:1,
    DesktopHeaderborderbuttonActive:"white",
    DesktopPAlignTextnextActive:"center",
    DButtonLeftActive:0,
    DButtonRightActive:0,
    DButtonTopActive:0,
    DButtonBottomActive:0,
    DNextpaddingLeftActive:0,
    DNextpaddingRightActive:0,
    DNextpaddingTopActive:0,
    DNextpaddingBottomActive:0,
    showsickerpickercolorActive:false,
    showtogglesettingActive:false,
    showmediumsettingActive:true,
    activeshowmediumsetting:true,
    TextopencolorsettingshowbuttonActive:false,
    opencolorsettingshowbuttonActive:false,
    showtogglesettingpadvnceasetting:false,
    DesktopfontsizeheaderButtonActive:16,
    TextDesktopHeaderbackgroundshowbuttonActive:"white",
    DesktopHeaderbackgroundshowbuttonActive:"transpartant",

    //
    NavDesktopHeaderborderbuttonActive:"#186AB4",
    NavTextDesktopHeaderbackgroundshowbuttonActive:"black",
    NavDesktopHeaderbackgroundshowbuttonActive:"transpartant",
    NavopencolorsettingshowbuttonActive:false,
    navbuttonwidth:107,
    navbuttonheight:47,
    NavDesktopPAlignTextnextActive:"center",
    NavDButtonLeftActive:0,
    NavDButtonRightActive:0,
    NavDButtonTopActive:0,
    NavDButtonBottomActive:0,
    NavDNextpaddingLeftActive:0,
    NavDNextpaddingRightActive:0,
    NavDNextpaddingTopActive:0,
    NavDNextpaddingBottomActive:0,
    NavshowsickerpickercolorActive:false,
    NavshowsickerpickercolorActive:false,
    NavnextbuttonborderAcitve:1,
    NavnextbuttonwidthActive:90,
    NavnextbuttonheightActive:31,
    NavshowtogglesettingActive:true,//asdasd
    showeditorback:false,
    NavshowmediumsettingActive:false,
    TextopencolorsettingshowbuttonActive:false,
    chatnav:"Lets Chat",
    NavDesktopheadfontweightNamebuttonActive:"Normal",
    NavDesktopheadfontweightbuttonActive:"Normal",
    NavDesktopfontsizeheaderButtonActive:18,
    NavshowmediumsettingActive:true,
    Navshowntbuttonactive:false,
    imagesettinghsow:false,
    Urlchange:"",
    showeditor:false,
    showeditor1:false,
    imagesettinghsow1:false,

}


handleResize = async() =>{
await this.setState({

  windowWidth: window.innerWidth
 
  
});
if(this.state.windowWidth<767)
{

console.log("hyderbad",destopheadertitle)

}
}

 componentDidMount=async()=>
 {
await this.getapicalled();
  await this.handleResize();
    window.addEventListener('resize', this.handleResize)


  // ReactGA.initialize('UA-157965665-1');
// console.log("sfdsfd",window.location.pathname + window.location.search);
await this.context.updatePathName(window.location.pathname + window.location.search)

 }
 

 inputValuestyingtyling=(e)=>
 {
this.setState({
  headertitleinput:e.currentTarget.textContent
})
 }

 inputValuestyingtyling1=(e)=>
 {
this.setState({
  headertitleinputpar:e.currentTarget.textContent
})
 }

 handlePlayPause = () => {
  this.setState({ playing: !this.state.playing })
}

handleStop = () => {
  this.setState({ url: null, playing: false })
}

handleToggleControls = () => {
  const url = this.state.url
  this.setState({
    controls: !this.state.controls,
    url: null
  }, () => this.load(url))
}

handleToggleLight = () => {
  this.setState({ light: !this.state.light })
}

handleToggleLoop = () => {
  this.setState({ loop: !this.state.loop })
}

handleVolumeChange = async(e) => {
  await this.setState({ volume: parseFloat(e.target.value) })
  console.log("voube",this.state.volume)
}

handleToggleMuted = () => {
  this.setState({ muted: !this.state.muted })
}

handleSetPlaybackRate = e => {
  this.setState({ playbackRate: parseFloat(e.target.value) }
  
  
  )
  console.log("playbackRate",this.sate.playbackRate)
}

handleTogglePIP = () => {
  this.setState({ pip: !this.state.pip })
}

// handlePlay = (state) => {
//   // console.log('onPlayz',this.state)
//   this.setState({ playing: true })

// }

handleEnablePIP = () => {
  console.log('onEnablePIP')
  this.setState({ pip: true })
}

handleDisablePIP = () => {
  console.log('onDisablePIP')
  this.setState({ pip: false })
}



handleSeekMouseDown = e => {
  this.setState({ seeking: true })
}

handleSeekChange = e => {
  this.setState({ played: parseFloat(e.target.value) })
}

handleSeekMouseUp = e => {
  this.setState({ seeking: false })
  this.player.seekTo(parseFloat(e.target.value))
}

// handleProgress = state => {
//  console.log('onProgress', state)
//   // We only want to update time slider if we are not currently seeking
//   if (!this.state.seeking) {
//     this.setState(state)
//   }
// }

handleEnded = () => {
  
  alert()
  this.setState({ playing: this.state.loop,
    active:true
  })
}

handleDuration = (duration) => {
  console.log('onDuration', duration)
  this.setState({ duration })
}
 

onEditorStateChange =(editorState) => {
  this.setState({
    editorState,
  });
};
componpent
onContentStateChange: Function = async(contentState) => {
  let kmaal=contentState
  await this.setState({
    contentEditext:kmaal
  })
}
componentWillUnmount() {
  window.removeEventListener('resize', this.handleResize)
}
mobilescreen=()=>
{
  this.setState({
    mobilewidth:"357px"

  })
}

headerfontsize=(item)=>
{
  this.setState({
    Desktopfontsizeheader:item
  })

}

headerfontsizepar=(item)=>
{
  this.setState({
    Desktopfontsizeheaderpar:item
  })

}
headerfontsizebutton=(item)=>
{
  this.setState({
    DesktopfontsizeheaderButton:item
  })

}

headerfontsizebuttonActive=(item)=>
{
  this.setState({
    DesktopfontsizeheaderButtonActive:item
  })

}

NavheaderfontsizebuttonActive=(item)=>
{
  this.setState({
    NavDesktopfontsizeheaderButtonActive:item
  })

}

handleChange=(e)=>
{

  this.setState({
    DesktopheadfontweightName:e.target.value
  })
  if(e.target.value==="Semi-bold")
  {
this.setState({
  Desktopheadfontweight:500

})
  }
  if(e.target.value==="Normal")
  {
this.setState({
  Desktopheadfontweight:100
})
  }
  if(e.target.value==="bold")
  {
this.setState({
  Desktopheadfontweight:"bold"
})
  }
}
handleChangepar=(e)=>
{

  this.setState({
    DesktopheadfontweightNamepar:e.target.value
  })
  if(e.target.value==="Semi-bold")
  {
this.setState({
  Desktopheadfontweightpar:500

})
  }
  if(e.target.value==="Normal")
  {
this.setState({
  Desktopheadfontweightpar:100
})
  }
  if(e.target.value==="bold")
  {
this.setState({
  Desktopheadfontweightpar:"bold"
})
  }
}


handleChangebutton=(e)=>
{

  this.setState({
    DesktopheadfontweightNamebutton:e.target.value
  })
  if(e.target.value==="Semi-bold")
  {
this.setState({
  Desktopheadfontweightbutton:500

})
  }
  if(e.target.value==="Normal")
  {
this.setState({
  Desktopheadfontweightbutton:100
})
  }
  if(e.target.value==="bold")
  {
this.setState({
  Desktopheadfontweightbutton:"bold"
})
  }
}



handleChangebuttonActive=(e)=>
{

  this.setState({
    DesktopheadfontweightNamebuttonActive:e.target.value
  })
  if(e.target.value==="Semi-bold")
  {
this.setState({
  DesktopheadfontweightbuttonActive:500

})
  }
  if(e.target.value==="Normal")
  {
this.setState({
  DesktopheadfontweightbuttonActive:100
})
  }
  if(e.target.value==="bold")
  {
this.setState({
  DesktopheadfontweightbuttonActive:"bold"
})
  }
}

NavhandleChangebuttonActive=(e)=>
{

  this.setState({
    NavDesktopheadfontweightNamebuttonActive:e.target.value
  })
  if(e.target.value==="Semi-bold")
  {
this.setState({
  NavDesktopheadfontweightbuttonActive:500

})
  }
  if(e.target.value==="Normal")
  {
this.setState({
  NavDesktopheadfontweightbuttonActive:100
})
  }
  if(e.target.value==="bold")
  {
this.setState({
  NavDesktopheadfontweightbuttonActive:"bold"
})
  }
}
handleChangeComplete = async (color) => {
  await this.setState({ DesktopHeaderbackground:color.hex });
};
handleChangeCompletepar = async (color) => {
  await this.setState({ DesktopHeaderbackgroundpar:color.hex });
}


handleChangeCompleteshowbutton = async (color) => {
  await this.setState({ DesktopHeaderbackgroundshowbutton:color.hex });
}

handleChangeCompleteshowbuttonActive = async (color) => {
  await this.setState({ DesktopHeaderbackgroundshowbuttonActive:color.hex });
}
NavhandleChangeCompleteshowbuttonActive = async (color) => {
  await this.setState({ NavDesktopHeaderbackgroundshowbuttonActive:color.hex });
}
TexthandleChangeCompleteshowbutton = async (color) => {
  await this.setState({ TextDesktopHeaderbackgroundshowbutton:color.hex });
}

TexthandleChangeCompleteshowbuttonActive = async (color) => {
  await this.setState({ TextDesktopHeaderbackgroundshowbuttonActive:color.hex });
}

NavTexthandleChangeCompleteshowbuttonActive = async (color) => {
  await this.setState({ NavTextDesktopHeaderbackgroundshowbuttonActive:color.hex });
}

handleChangeCompleteforborder= async (color) => {
  await this.setState({ DesktopHeaderborderbutton:color.hex });
}
handleChangeCompleteforborderActive= async (color) => {
  await this.setState({ DesktopHeaderborderbuttonActive:color.hex });
}
NavhandleChangeCompleteforborderActivebackgrond= async (color) => {
  await this.setState({ DesktopHeaderborderbuttonbackgrond:color.hex });
}

colorsettingopen=()=>
{
  this.setState({
    opencolorsetting:!this.state.opencolorsetting
  })
}
colorsettingopenpar=()=>
{
  this.setState({
    opencolorsettingpar:!this.state.opencolorsettingpar
  })
}
colorsettingopenshowbutton=()=>
{
  this.setState({
    opencolorsettingshowbutton:!this.state.opencolorsettingshowbutton,
    Textopencolorsettingshowbutton:false,
  })
}

colorsettingopenshowbuttonActive=()=>
{
  this.setState({
    opencolorsettingshowbuttonActive:!this.state.opencolorsettingshowbuttonActive,
    TextopencolorsettingshowbuttonActive:false,
  })
}
NavcolorsettingopenshowbuttonActive=()=>
{
  this.setState({
    NavopencolorsettingshowbuttonActive:!this.state.NavopencolorsettingshowbuttonActive,
    NavTextopencolorsettingshowbuttonActive:false,
  })
}
Textcolorsettingopenshowbutton=()=>
{
  this.setState({
    Textopencolorsettingshowbutton:!this.state.Textopencolorsettingshowbutton,
    opencolorsettingshowbutton:false,
  })
}

TextcolorsettingopenshowbuttonActive=()=>
{
  this.setState({
    TextopencolorsettingshowbuttonActive:!this.state.TextopencolorsettingshowbuttonActive,
    opencolorsettingshowbuttonActive:false,
  })
}
NavTextcolorsettingopenshowbuttonActive=()=>
{
  this.setState({
    NavTextopencolorsettingshowbuttonActive:!this.state.NavTextopencolorsettingshowbuttonActive,
    NavopencolorsettingshowbuttonActive:false,
  })
}
inputValuestying = async e => {
let n1 = e.target
await this.setState({
    [n1.name]: n1.value,
  });
  
}
advancesettingtoggle=()=>{
  this.setState({
    showtogglesetting:!this.state.showtogglesetting,
    showmediumsetting:!this.state.showmediumsetting,
  })
}
advancesettingtogglepar=()=>{
  this.setState({
    showtogglesettingpar:!this.state.showtogglesettingpar,
    showmediumsettingpar:!this.state.showmediumsettingpar,
  })
}
advancesettingtoggleparActive=()=>{
  this.setState({
    showtogglesettingActive:!this.state.showtogglesettingActive,
    showmediumsettingActive:!this.state.showmediumsettingActive,
   
  })
}

NavadvancesettingtoggleparActive=()=>{
  this.setState({
    NavshowtogglesettingActive:!this.state.NavshowtogglesettingActive,
    NavshowmediumsettingActive:!this.state.NavshowmediumsettingActive,
   
  })
}
bacavdancesetting=()=>
{
  this.setState({
    showtogglesetting:!this.state.showtogglesetting,
    showmediumsetting:!this.state.showmediumsetting
  })
}

bacavdancesettingpar=()=>
{
  this.setState({
    showtogglesettingpar:!this.state.showtogglesettingpar,
    showmediumsettingpar:!this.state.showmediumsettingpar
  })
}
bacavdancesettingActive=()=>
{
  this.setState({
    showtogglesettingActive:!this.state.showtogglesettingActive,
    showmediumsettingActive:!this.state.showmediumsettingActive
  })
}

NavbacavdancesettingActive=()=>
{
  this.setState({
    NavshowtogglesettingActive:!this.state.NavshowtogglesettingActive,
    NavshowmediumsettingActive:!this.state.NavshowmediumsettingActive
  })
}


Desktopheaderalignmentfunction=(item)=>
{
this.setState({
  DesktopAlignText:item
})
}
Desktopparagraphalignmentfunction=(item)=>
{
this.setState({
  DesktopPAlignTextpar:item
})
}
Desktopparagraphalignmentfunctionnext=(item)=>
{
this.setState({
  DesktopPAlignTextnext:item
})
}
DesktopparagraphalignmentfunctionnextActive=(item)=>
{
this.setState({
  DesktopPAlignTextnextActive:item
})
}
NavDesktopparagraphalignmentfunctionnextActive=(item)=>
{
this.setState({
  NavDesktopPAlignTextnextActive:item
})
}

onStart = () => {
  this.setState({activeDrags: ++this.state.activeDrags});
};

onStop = () => {
  this.setState({activeDrags: --this.state.activeDrags});
};
setdropshowoutside=()=>
{
  this.setState({
    showmediumsettingshow:false,
  })
}
setdropshowoutsidepar=()=>
{
  this.setState({
    showmediumsettingshowpar:false,
  })
}


setdropshowoutsideTrue=()=>
{
  this.setState({
    showmediumsettingshow:true,
    showmediumsetting:true,
    showtogglesetting:false,
  
  })
}
setdropshowoutsideTruepar=()=>
{
  alert()
  this.setState({
    showmediumsettingshowpar:true,
    showmediumsettingpar:true,
    showtogglesettingpar:false,

  })
}
// onChangeHandler = (event) => {
//   this.setState({
//       selectedFile: event.target.files[0],
//       loaded: 0,
//   });
//  ;
// };
onChangeHandler= async(e)=> {
  

  let reader = new FileReader();
  let file = e.target.files[0];


  reader.onloadend = async () => {
   await this.setState({
  
    selectedFile: reader.result,
   
    });
  }

  reader.readAsDataURL(file)
}

handleSubmit = (event) => {
  event.preventDefault();
  // const formData = new FormData();
  // const { selectedFile } = this.state;
  // formData.append('inputFile', selectedFile);
  // fetch('/api/upload', {
  //     method: 'POST',
  //     body: formData,
  // });
};
colorbordershowfun=()=>
{
  this.setState({
    showsickerpickercolor:!this.state.showsickerpickercolor
  })
}
colorbordershowfunActive=()=>
{
  this.setState({
    showsickerpickercolorActive:!this.state.showsickerpickercolorActive
  })
}

NavcolorbordershowfunActive=()=>
{
  this.setState({
    NavshowsickerpickercolorActive:!this.state.NavshowsickerpickercolorActive
  })

}
advancesettingtogglenext=()=>
{
  this.setState({
    showbuttonsettingrich:false,
    showtogglesettingpadvnceasetting:true
  })
}
bacavdancesettingnext=()=>
{
  this.setState({
    showbuttonsettingrich:true,
    showtogglesettingpadvnceasetting:false
  })
}
closenoactive=()=>
{
  alert()
  this.setState({
    shownxtbutton:false
  })
}
noactivenextbutton=()=>
{
  this.setState({
    active:true,
    shownxtbutton:true,
    showclassactive:false,
  })
}
activenextbutton=()=>
{
 
  this.setState({
    active:false,
    showntbuttonactive:true,
    showclassactive:false
  })
}
closeactivebuttonstate=()=>
{
  this.setState({
    showntbuttonactive:false,
  })
}
Navcloseactivebuttonstate=()=>
{
  this.setState({
    Navshowntbuttonactive:false,
  })
}

showactivesection=()=>
{
  this.setState({
    showclassactive:true,
  })
}
handleImageChangeimage= async(e)=> {
  

  let reader = new FileReader();
  let file = e.target.files[0];


  reader.onloadend = async () => {
   await this.setState({
  
      imagePreviewUrl: reader.result,
  
    });
  }

  reader.readAsDataURL(file)
}
nabvarchat=()=>
{
  this.setState({
    Navshowntbuttonactive:true,
    imagesettinghsow:false,
  })
}
imagesettingshow=()=>
{
  this.setState({
    imagesettinghsow:true
  })
}
imagesettingshowfu=()=>
{
  this.setState({
    imagesettinghsow1:true
  })
}
editsection=()=>
{
  this.setState({
    showeditor:true,
  })
}
editsection1=()=>
{
  this.setState({
    showeditor1:true,
  })
}
backsetting=()=>
{
  this.setState({
    showeditorback:true,
  })
}

postcustomdata=async()=>
{
  // fontSize:,
  // fontWeight:,
  
 
  // paddingLeft:`${}px`,
  // paddingTop:`${}px`,
  // paddingBottom:`${}px`,
  // paddingRight:`${}px`,
  // height:`${}px`,
  // textAlign:,
  
  // marginLeft:`${}px`,
  // marginTop:`${this.state.}px`,
  // marginBottom:`${this.state.}px`,
  // marginRight:`${this.state.}px`,
let application={
homewhatisgxNav:[
  {
navimg:this.state.imagePreviewUrl,
inlinestyle:{
 imagewidth:this.state.navbuttonwidth,
 imageheight:this.state.navbuttonheight,
},

  },
  {
     buttonnamenav:this.state.chatnav,
     navbuttonurl:this.state.Urlchange,
     inlinestyle:{
      navbuttonfont:this.state.NavDesktopfontsizeheaderButtonActive,
      navbuttonfontweight:this.state.NavDesktopheadfontweightbuttonActive,
      Navbuttoncolor:this.state.NavTextDesktopHeaderbackgroundshowbuttonActive,
      Navbuttonbackground:this.state.NavDesktopHeaderbackgroundshowbuttonActive,
      navbuttonwidth:this.state.NavnextbuttonwidthActive,
      navbuttonheight:this.state.NavnextbuttonheightActive,
      navbuttonbroderpx:this.state.NavnextbuttonborderAcitve, 
      navbuttonbrodercolor:this.state.NavDesktopHeaderborderbuttonActive,
      navbuttonPL:this.state.NavDNextpaddingLeftActive,
      navbuttonPT:this.state.NavDNextpaddingTopActive,
      navbuttonPB:this.state.NavDNextpaddingBottomActive,
      navbuttonPR:this.state.NavDNextpaddingRightActive,
      navbuttonML:this.state.NavDButtonLeftActive,
      navbuttonMT:this.state.NavDButtonTopActive,
      navbuttonMB:this.state.NavDButtonBottomActive,
      navbuttonMR:this.state.NavDButtonRightActive,
      navbauttonalign:this.state.NavDesktopPAlignTextnextActive,
     }
  }

],
//end of nav
homewhatgxheader:[
  {
   headername:this.state.headertitleinput,
    inlinestyle:{
    HfontSize:this.state.Desktopfontsizeheader,
    HfontWeight:this.state.Desktopheadfontweight,
    Hcolor:this.state.DesktopHeaderbackground,
    HpaddingLeft:this.state.DHpaddingLeft,
    HpaddingTop:this.state.DHpaddingTop,
    HpaddingBottom:this.state.DHpaddingBottom,
    HpaddingRight:this.state.DHpaddingRight,
    HtextAlign:this.state.DesktopAlignText,
    HmarginLeft:this.state.DHMLeft,
    HmarginTop:this.state.DHMTop,
    HmarginBottom:this.state.DHMBottom,
    HmarginRight:this.state.DHMRight,
   }
 }
],
homewhatgxparagrah:[
{
  paragraphname:this.state.headertitleinputpar      ,
inlinestyle:{
   ParfontSize:this.state.Desktopfontsizeheaderpar,
   ParfontWeight:this.state.Desktopheadfontweightpar,
   Parcolor:this.state.DesktopHeaderbackgroundpar,
   ParpaddingLeft:this.state.DHMpaddingLeft,
   ParpaddingTop:this.state.DHMpaddingTop,
   ParpaddingBottom:this.state.DHMpaddingBottom,
   ParpaddingRight:this.state.DHMpaddingRight,
   PartextAlign:this.state.DesktopPAlignTextpar,
   ParmarginLeft:this.state.DHPLeft,
   ParmarginTop:this.state.DHPTop,
   ParmarginBottom:this.state.DHPBottom,
   ParmarginRight:this.state.DHPRight,
},

}
],

// destopkbutton={
//   fontSize:this.state.DesktopfontsizeheaderButton,
//   fontWeight:this.state.DesktopheadfontweightNamebutton,
//   color:this.state.TextDesktopHeaderbackgroundshowbutton,
//   background:this.state.DesktopHeaderbackgroundshowbutton,
//   width:`${this.state.nextbuttonwidth}px`,
//   paddingLeft:`${this.state.DNextpaddingLeft}px`,
//   paddingTop:`${this.state.DNextpaddingTop}px`,
//   paddingBottom:`${this.state.DNextpaddingBottom}px`,
//   paddingRight:`${this.state.DNextpaddingRight}px`,
//   height:`${this.state.nextbuttonheight}px`,
//   textAlign:this.state.DesktopPAlignTextnext,
//   border:`${this.state.nextbuttonborder}px solid ${this.state.DesktopHeaderborderbutton}`,
//   marginLeft:`${this.state.DButtonLeft}px`,
//   marginTop:`${this.state.DButtonTop}px`,
//   marginBottom:`${this.state.DButtonBottom}px`,
//   marginRight:`${this.state.DButtonRight}px`,
// }
homewhatgxbutton:[
  {
buttonname:this.state.ButtonTextnext,
buttonnamenoactive:this.state.ButtonTextnextnoActive,
inlinestyle:{
   BfontSize:this.state.DesktopfontsizeheaderButton,
   BfontWeight:this.state.DesktopheadfontweightNamebutton,
   Bcolor:this.state.TextDesktopHeaderbackgroundshowbutton,
   Bbackground:this.state.DesktopHeaderbackgroundshowbutton,
   Bwidth:this.state.nextbuttonwidth,
   BpaddingLeft:this.state.DNextpaddingLeft,
   BpaddingTop:this.state.DNextpaddingTop,
   BpaddingBottom:this.state.DNextpaddingBottom,
   BpaddingRight:this.state.DNextpaddingRight,
   Bheight:this.state.nextbuttonheight,
   BtextAlign:this.state.DesktopPAlignTextnext,
   Bborder:this.state.nextbuttonborder,
   Bcolorborder:this.state.DesktopHeaderborderbutton,
   BmarginLeft:this.state.DButtonLeft,
   BmarginTop:this.state.DButtonTop,
   BmarginBottom:this.state.DButtonBottom,
   BmarginRight:this.state.DButtonRight,
//noactive
NfontSize:this.state.DesktopfontsizeheaderButtonActive,
NfontWeight:this.state.DesktopheadfontweightNamebuttonActive,
Ncolor:this.state.TextDesktopHeaderbackgroundshowbuttonActive,
Nbackground:this.state.DesktopHeaderbackgroundshowbuttonActive,
Nwidth:this.state.nextbuttonwidthActive,
NpaddingLeft:this.state.DNextpaddingLeftActive,
NpaddingTop:this.state.DNextpaddingTopActive,
NpaddingBottom:this.state.DNextpaddingBottomActive,
NpaddingRight:this.state.DNextpaddingRightActive,
Nheight:this.state.nextbuttonheightActive,
NtextAlign:this.state.DesktopPAlignTextnextActive,
Nborder:this.state.nextbuttonborderAcitve,
NborderC:this.state.DesktopHeaderborderbuttonActive,
NmarginLeft:this.state.DButtonLeftActive,
NmarginTop:this.state.DButtonTopActive,
NmarginBottom:this.state.DButtonBottomActive,
NmarginRight:this.state.DButtonRightActive,









}
  }
],
videostream:[
  {
    name:this.state.selectedFile,
    back:this.state.DesktopHeaderborderbuttonbackgrond
  }
]
  }
  await axios.post("https://vsa.apimachine.com/gxtheme", {
   custom_scheme:application
  })
  .then(r => {
    if (r.data) {
      alert("Updated...");
  console.log("manup",r.data)

  
    } 
    
  });
}

getapicalled=async()=>
{
  await axios.get("https://vsa.apimachine.com/gxtheme/latest")
  .then (r => {
    if (r.data) {
      console.log("console",r.data.data.custom_scheme.videostream[0].name)
   
      // navbuttonfontweight:this.state.NavDesktopheadfontweightbuttonActive,

      // Navbuttoncolor:this.state.NavTextDesktopHeaderbackgroundshowbuttonActive,


      // Navbuttonbackground:this.state.NavDesktopHeaderbackgroundshowbuttonActive,

      // navbuttonwidth:this.state.NavnextbuttonwidthActive,


      // navbuttonheight:this.state.NavnextbuttonheightActive,


      // navbuttonbroderpx:this.state.NavnextbuttonborderAcitve, 

      // navbuttonbrodercolor:this.state.NavDesktopHeaderborderbuttonActive,

      // navbuttonPL:this.state.NavDNextpaddingLeftActive,


      // navbuttonPT:this.state.NavDNextpaddingTopActive,

      // navbuttonPB:this.state.NavDNextpaddingBottomActive,

      // navbuttonPR:this.state.NavDNextpaddingRightActive,


      // navbuttonML:this.state.NavDButtonLeftActive,
      // navbuttonMT:this.state.NavDButtonTopActive,
      // navbuttonMB:this.state.NavDButtonBottomActive,
      // navbuttonMR:this.state.NavDButtonRightActive,
      // navbauttonalign:this.state.NavDesktopPAlignTextnextActive,
 this.setState({
  imagePreviewUrl:r.data.data.custom_scheme.homewhatisgxNav[0].navimg,
  navbuttonheight:r.data.data.custom_scheme.homewhatisgxNav[0].inlinestyle.imageheight,
  imagewidth:r.data.data.custom_scheme.homewhatisgxNav[0].inlinestyle.imagewidth,
  chatnav:r.data.data.custom_scheme.homewhatisgxNav[1].buttonnamenav,
  // nav
  NavDesktopfontsizeheaderButtonActive:r.data.data.custom_scheme.homewhatisgxNav[1].inlinestyle.navbuttonfont,
  NavDesktopheadfontweightbuttonActive:r.data.data.custom_scheme.homewhatisgxNav[1].inlinestyle.navbuttonfontweight,
NavTextDesktopHeaderbackgroundshowbuttonActive:r.data.data.custom_scheme.homewhatisgxNav[1].inlinestyle.Navbuttoncolor,
NavDesktopHeaderbackgroundshowbuttonActive:r.data.data.custom_scheme.homewhatisgxNav[1].inlinestyle.Navbuttonbackground,
NavnextbuttonwidthActive:r.data.data.custom_scheme.homewhatisgxNav[1].inlinestyle.navbuttonwidth,
NavnextbuttonheightActive:r.data.data.custom_scheme.homewhatisgxNav[1].inlinestyle.navbuttonheight,
NavnextbuttonborderAcitve:r.data.data.custom_scheme.homewhatisgxNav[1].inlinestyle.navbuttonbroderpx,
NavDesktopHeaderborderbuttonActive:r.data.data.custom_scheme.homewhatisgxNav[1].inlinestyle.navbuttonbrodercolor,
NavDNextpaddingLeftActive:r.data.data.custom_scheme.homewhatisgxNav[1].inlinestyle.navbuttonPL,
NavDNextpaddingTopActive:r.data.data.custom_scheme.homewhatisgxNav[1].inlinestyle.navbuttonPT,
NavDNextpaddingBottomActive:r.data.data.custom_scheme.homewhatisgxNav[1].inlinestyle.navbuttonPB,
NavDNextpaddingRightActive:r.data.data.custom_scheme.homewhatisgxNav[1].inlinestyle.navbuttonPR,
NavDButtonLeftActive:r.data.data.custom_scheme.homewhatisgxNav[1].inlinestyle.navbuttonML,
NavDButtonTopActive:r.data.data.custom_scheme.homewhatisgxNav[1].inlinestyle.navbuttonMT,
NavDButtonBottomActive:r.data.data.custom_scheme.homewhatisgxNav[1].inlinestyle.navbuttonMB,
NavDButtonRightActive:r.data.data.custom_scheme.homewhatisgxNav[1].inlinestyle.navbuttonMR,
NavDesktopPAlignTextnextActive:r.data.data.custom_scheme.homewhatisgxNav[1].inlinestyle.navbauttonalign,

//header
headertitleinput:r.data.data.custom_scheme.homewhatgxheader[0].headername,
Desktopfontsizeheader:r.data.data.custom_scheme.homewhatgxheader[0].inlinestyle.HfontSize,
Desktopheadfontweight:r.data.data.custom_scheme.homewhatgxheader[0].inlinestyle.HfontWeight,
DesktopHeaderbackground:r.data.data.custom_scheme.homewhatgxheader[0].inlinestyle.Hcolor,
DHpaddingLeft:r.data.data.custom_scheme.homewhatgxheader[0].inlinestyle.HpaddingLeft,
DHpaddingTop:r.data.data.custom_scheme.homewhatgxheader[0].inlinestyle.HpaddingTop,
DHpaddingBottom:r.data.data.custom_scheme.homewhatgxheader[0].inlinestyle.HpaddingBottom,
DHpaddingRight:r.data.data.custom_scheme.homewhatgxheader[0].inlinestyle.HpaddingRight,
DesktopAlignText:r.data.data.custom_scheme.homewhatgxheader[0].inlinestyle.HtextAlign,
DHMLeft:r.data.data.custom_scheme.homewhatgxheader[0].inlinestyle.HmarginLeft,
DHMTop:r.data.data.custom_scheme.homewhatgxheader[0].inlinestyle.HmarginTop,
DHMBottom:r.data.data.custom_scheme.homewhatgxheader[0].inlinestyle.HmarginBottom,
DHMRight:r.data.data.custom_scheme.homewhatgxheader[0].inlinestyle.HmarginRight,

//paragraph
headertitleinputpar:r.data.data.custom_scheme.homewhatgxparagrah[0].paragraphname,
Desktopfontsizeheaderpar:r.data.data.custom_scheme.homewhatgxparagrah[0].inlinestyle.ParfontSize,
Desktopheadfontweightpar:r.data.data.custom_scheme.homewhatgxparagrah[0].inlinestyle.ParfontWeight,
DesktopHeaderbackgroundpar:r.data.data.custom_scheme.homewhatgxparagrah[0].inlinestyle.Parcolor,
DHMpaddingLeft:r.data.data.custom_scheme.homewhatgxparagrah[0].inlinestyle.ParpaddingLeft,
DHMpaddingTop:r.data.data.custom_scheme.homewhatgxparagrah[0].inlinestyle.ParpaddingTop,
DHMpaddingBottom:r.data.data.custom_scheme.homewhatgxparagrah[0].inlinestyle.ParpaddingBottom,
DHMpaddingRight:r.data.data.custom_scheme.homewhatgxparagrah[0].inlinestyle.ParpaddingRight,
DesktopPAlignTextpar:r.data.data.custom_scheme.homewhatgxparagrah[0].inlinestyle.PartextAlign,
DHPLeft:r.data.data.custom_scheme.homewhatgxparagrah[0].inlinestyle.ParmarginLeft,
DHPTop:r.data.data.custom_scheme.homewhatgxparagrah[0].inlinestyle.ParmarginTop,
DHPBottom:r.data.data.custom_scheme.homewhatgxparagrah[0].inlinestyle.ParmarginBottom,
DHPRight:r.data.data.custom_scheme.homewhatgxparagrah[0].inlinestyle.ParmarginRight,

//buttonactive

ButtonTextnext:r.data.data.custom_scheme.homewhatgxbutton[0].buttonname,
ButtonTextnextnoActive:r.data.data.custom_scheme.homewhatgxbutton[0].buttonnamenoactive,
//:this.state.,
//    :this.state.,
//    :this.state.,
//    :this.state.,
//    :this.state.,
//    :this.state.,
//    :this.state.,
//    :this.state.,
//    :this.state.,
//    :this.state.,
//    :this.state.,
//    :this.state.,
//    :this.state.,
//    :this.state.,
//    :this.state.,
//    :this.state.,
DesktopfontsizeheaderButton:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.BfontSize,
DesktopheadfontweightNamebutton:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.BfontWeight,
TextDesktopHeaderbackgroundshowbutton:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.Bcolor,
DesktopHeaderbackgroundshowbutton:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.Bbackground,
nextbuttonwidth:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.Bwidth,
DNextpaddingLeft:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.BpaddingLeft,
DNextpaddingTop:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.BpaddingTop,
DNextpaddingBottom:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.BpaddingBottom,
DNextpaddingRight:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.BpaddingRight,
nextbuttonheight:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.Bheight,
DesktopPAlignTextnext:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.BtextAlign,
nextbuttonborder:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.Bborder,
DesktopHeaderborderbutton:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.Bcolorborder,
DButtonLeft:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.BmarginLeft,
DButtonTop:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.BmarginTop,
DButtonBottom:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.BmarginBottom,
DButtonRight:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.BmarginRight,  

// noa


// NfontSize:this.state.DesktopfontsizeheaderButtonActive,
// NfontWeight:this.state.DesktopheadfontweightNamebuttonActive,
// Ncolor:this.state.TextDesktopHeaderbackgroundshowbuttonActive,
// Nbackground:this.state.DesktopHeaderbackgroundshowbuttonActive,
// Nwidth:this.state.nextbuttonwidthActive,
// NpaddingLeft:this.state.DNextpaddingLeftActive,
// NpaddingTop:this.state.DNextpaddingTopActive,
// NpaddingBottom:this.state.DNextpaddingBottomActive,
// NpaddingRight:this.state.DNextpaddingRightActive,
// Nheight:this.state.nextbuttonheightActive,

// :this.state.DesktopPAlignTextnextActive,
// Nborder:this.state.nextbuttonborderAcitve,
// NborderC:this.state.,
// NmarginLeft:this.state.DButtonLeftActive,
// NmarginTop:this.state.DButtonTopActive,
// NmarginBottom:this.state.DButtonBottomActive,
// NmarginRight:this.state.DButtonRightActive,



DesktopfontsizeheaderButtonActive:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.NfontSize,
DesktopheadfontweightNamebuttonActive:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.NfontWeight,
TextDesktopHeaderbackgroundshowbuttonActive:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.Ncolor,
DesktopHeaderbackgroundshowbuttonActive:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.Nbackground,
nextbuttonwidthActive:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.Nwidth,
DNextpaddingLeftActive:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.NpaddingLeft,
DNextpaddingTopActive:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.NpaddingTop,
DNextpaddingBottomActive:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.NpaddingBottom,
DNextpaddingRightActive:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.NpaddingRight,
nextbuttonheightActive:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.Nheight,
DesktopPAlignTextnextActive:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.NtextAlign,
DesktopHeaderborderbuttonActive:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.NborderC,
DButtonLeftActive:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.NmarginLeft,
DButtonTopActive:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.NmarginTop,
DButtonBottomActive:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.NmarginBottom,
DButtonRightActive:r.data.data.custom_scheme.homewhatgxbutton[0].inlinestyle.NmarginRight,

//stee
selectedFile:r.data.data.custom_scheme.videostream[0].name,
DesktopHeaderborderbuttonbackgrond:r.data.data.custom_scheme.videostream[0].back

// homewhatgxbutton:[
//   {
// buttonname:this.state.,
// buttonnamenoactive:this.state.,
// inlinestyle:{
//    
//    BfontWeight:this.state.DesktopheadfontweightNamebutton,
//    Bcolor:this.state.TextDesktopHeaderbackgroundshowbutton,
//    Bbackground:this.state.DesktopHeaderbackgroundshowbutton,
//    Bwidth:this.state.nextbuttonwidth,
//    BpaddingLeft:this.state.DNextpaddingLeft,
//    BpaddingTop:this.state.DNextpaddingTop,
//    BpaddingBottom:this.state.DNextpaddingBottom,
//    BpaddingRight:this.state.DNextpaddingRight,
//    Bheight:this.state.nextbuttonheight,
//    BtextAlign:this.state.DesktopPAlignTextnext,
//    Bborder:this.state.nextbuttonborder,
//    Bcolorborder:this.state.DesktopHeaderborderbutton,
//    BmarginLeft:this.state.DButtonLeft,
//    BmarginTop:this.state.DButtonTop,
//    BmarginBottom:this.state.DButtonBottom,
//    BmarginRight:this.state.DButtonRight,
// //noactive
// NfontSize:this.state.DesktopfontsizeheaderButtonActive,
// NfontWeight:this.state.DesktopheadfontweightNamebuttonActive,
// Ncolor:this.state.TextDesktopHeaderbackgroundshowbuttonActive,
// Nbackground:this.state.DesktopHeaderbackgroundshowbuttonActive,
// Nwidth:this.state.nextbuttonwidthActive,
// NpaddingLeft:this.state.DNextpaddingLeftActive,
// NpaddingTop:this.state.DNextpaddingTopActive,
// NpaddingBottom:this.state.DNextpaddingBottomActive,
// NpaddingRight:this.state.DNextpaddingRightActive,
// Nheight:this.state.nextbuttonheightActive,
// NtextAlign:this.state.DesktopPAlignTextnextActive,
// Nborder:this.state.nextbuttonborderAcitve,
// NborderC:this.state.DesktopHeaderborderbuttonActive,
// NmarginLeft:this.state.DButtonLeftActive,
// NmarginTop:this.state.DButtonTopActive,
// NmarginBottom:this.state.DButtonBottomActive,
// NmarginRight:this.state.DButtonRightActive,


})

  
    } 
    
  })

}


render() {
  destopheadertitle=
  {
    fontSize:this.state.Desktopfontsizeheader,
    fontWeight:this.state.Desktopheadfontweight,
    color:this.state.DesktopHeaderbackground,
    
  
  }
  destopheadertitleDIv={
    paddingLeft:`${this.state.DHpaddingLeft}%`,
    paddingTop:`${this.state.DHpaddingTop}%`,
    paddingBottom:`${this.state.DHpaddingBottom}%`,
    paddingRight:`${this.state.DHpaddingRight}%`,
    textAlign:this.state.DesktopAlignText,
    marginLeft:`${this.state.DHMLeft}%`,
    marginTop:`${this.state.DHMTop}%`,
    marginBottom:`${this.state.DHMBottom}%`,
    marginRight:`${this.state.DHMRight}%`,
  }



  destopheadertitlepar=
  {
    fontSize:this.state.Desktopfontsizeheaderpar,
    fontWeight:this.state.Desktopheadfontweightpar,
    color:this.state.DesktopHeaderbackgroundpar,
    
  
  }
  destopheadertitleDIvpar={
    paddingLeft:`${this.state.DHMpaddingLeft}%`,
    paddingTop:`${this.state.DHMpaddingTop}%`,
    paddingBottom:`${this.state.DHMpaddingBottom}%`,
    paddingRight:`${this.state.DHMpaddingRight}%`,
    textAlign:this.state.DesktopPAlignTextpar,
    marginLeft:`${this.state.DHPLeft}%`,
    marginTop:`${this.state.DHPTop}%`,
    marginBottom:`${this.state.DHPBottom}%`,
    marginRight:`${this.state.DHPRight}%`,
  }


  
  destopkbutton={
    fontSize:this.state.DesktopfontsizeheaderButton,
    fontWeight:this.state.DesktopheadfontweightNamebutton,
    color:this.state.TextDesktopHeaderbackgroundshowbutton,
    background:this.state.DesktopHeaderbackgroundshowbutton,
    width:`${this.state.nextbuttonwidth}px`,
    paddingLeft:`${this.state.DNextpaddingLeft}%`,
    paddingTop:`${this.state.DNextpaddingTop}%`,
    paddingBottom:`${this.state.DNextpaddingBottom}%`,
    paddingRight:`${this.state.DNextpaddingRight}%`,
    height:`${this.state.nextbuttonheight}px`,
    textAlign:this.state.DesktopPAlignTextnext,
    border:`${this.state.nextbuttonborder}px solid ${this.state.DesktopHeaderborderbutton}`,
    marginLeft:`${this.state.DButtonLeft}%`,
    marginTop:`${this.state.DButtonTop}%`,
    marginBottom:`${this.state.DButtonBottom}%`,
    marginRight:`${this.state.DButtonRight}%`,
  }

  destopkbuttonActive={
    fontSize:this.state.DesktopfontsizeheaderButtonActive,
    fontWeight:this.state.DesktopheadfontweightNamebuttonActive,
    color:this.state.TextDesktopHeaderbackgroundshowbuttonActive,
    background:this.state.DesktopHeaderbackgroundshowbuttonActive,
    width:`${this.state.nextbuttonwidthActive}px`,
    paddingLeft:`${this.state.DNextpaddingLeftActive}%`,
    paddingTop:`${this.state.DNextpaddingTopActive}%`,
    paddingBottom:`${this.state.DNextpaddingBottomActive}%`,
    paddingRight:`${this.state.DNextpaddingRightActive}%`,
    height:`${this.state.nextbuttonheightActive}px`,
    textAlign:this.state.DesktopPAlignTextnextActive,
    border:`${this.state.nextbuttonborderAcitve}px solid ${this.state.DesktopHeaderborderbuttonActive}`,
    marginLeft:`${this.state.DButtonLeftActive}%`,
    marginTop:`${this.state.DButtonTopActive}%`,
    marginBottom:`${this.state.DButtonBottomActive}%`,
    marginRight:`${this.state.DButtonRightActive}%`,
  }
  destopkbuttonActiveNavbar={
    fontSize:this.state.NavDesktopfontsizeheaderButtonActive,
    fontWeight:this.state.NavDesktopheadfontweightbuttonActive,
    color:this.state.NavTextDesktopHeaderbackgroundshowbuttonActive,
    background:this.state.NavDesktopHeaderbackgroundshowbuttonActive,
    width:`${this.state.NavnextbuttonwidthActive}px`,
    paddingLeft:`${this.state.NavDNextpaddingLeftActive}%`,
    paddingTop:`${this.state.NavDNextpaddingTopActive}%`,
    paddingBottom:`${this.state.NavDNextpaddingBottomActive}%`,
    paddingRight:`${this.state.NavDNextpaddingRightActive}%`,
    height:`${this.state.NavnextbuttonheightActive}px`,
    textAlign:this.state.NavDesktopPAlignTextnextActive,
    border:`${this.state.NavnextbuttonborderAcitve}px solid ${this.state.NavDesktopHeaderborderbuttonActive}`,
    marginLeft:`${this.state.NavDButtonLeftActive}%`,
    marginTop:`${this.state.NavDButtonTopActive}%`,
    marginBottom:`${this.state.NavDButtonBottomActive}%`,
    marginRight:`${this.state.NavDButtonRightActive}%`,
  }

//  let mobileheadertitle=
//   {
//     fontSize:this.state.Desktopfontsizeheader,
//     fontWeight:this.state.Desktopheadfontweight,
//     color:this.state.DesktopHeaderbackground
//   }


console.log("Played",this.state.headertitleinput)
const { url, controls, light, volume, muted, loop, loaded, duration, pip } = this.state
const dragHandlers = {onStart: this.onStart, onStop: this.onStop};
    return (
      <ProductConsumer>
        {context_data => {
          const {
            handleShow,handleClose, show,inputValue,OnBuffer,played,handlePause,handleProgress,VideoOnPlay,VideoonPause,sendData,videoOnePlay,onPlaybackRateChangedata,VideoOnwatch,NameFunction,SendAllData,handlePlay,playing,onstartfun,OnseekFun,EmailSendcall,submitedata,Emailsubmitedata,next,
stopPageOneTimmer,
 } = context_data;
 console.log("voube",this.state.volume)
  return (
  <>

 


  

  {
    this.state.imagesettinghsow?
    <Draggable bounds="parent" defaultPosition={{x: 0, y: 0}} {...dragHandlers} >

    <div className="navbardraaging">
  
 
 <h1 className="dvancestting">Image Setting</h1>
 
 <div class="upload-btn-wrapper">
 <button class="btn-vsa">Please Upload Your Logo</button>
 <input type="file" name="myfile" onChange={(e)=>this.handleImageChangeimage(e)} />
 </div>
 <div className="labeltextalignmentdiv">
 <h1 className="adavncesetiontitle">Width</h1>
 <label className="labelpx">px</label>
 </div>
 
 
 <div className="advancesetting">
   <div className="divpaddingsection">
   <input type="number" name="navbuttonwidth" min="0" value={this.state.navbuttonwidth} onChange={this.inputValuestying}/>
  
   </div>
 
   </div>
  
   <div className="labeltextalignmentdiv">
 <h1 className="adavncesetiontitle">Height</h1>
 <label className="labelpx">px</label>
 </div>
 
 <div className="advancesetting">
   <div className="divpaddingsection">
   <input type="number" name="navbuttonheight" min="0" value={this.state.navbuttonheight} onChange={this.inputValuestying}/>
   </div>
   </div>
  
   <h1 className="adavncesetiontitle" style={{cursor:"pointer"}}  onClick={this.nabvarchat}>Button Controller <i class="fas fa-hand-point-right"></i></h1>
   <h1 className="adavncesetiontitle" style={{cursor:"pointer"}}  onClick={this.nabvarchat}>Navbar Controller <i class="fas fa-hand-point-right"></i></h1>
    </div>
  </Draggable>
    :null
  }



{/* button letscharts */}
{
     this.state.Navshowntbuttonactive?
     <>
<ClickAwayListener onClickAway={this.Navcloseactivebuttonstate} > 
 <Draggable bounds="parent" defaultPosition={{x: 0, y: 0}} {...dragHandlers} >
             
           
 <div className="d-flex">

    {
     this.state.NavshowmediumsettingActive? 
     <>
  

<div className="richtextbox">
  <div className="avdancesettinsetion">
<i class="fas fa-cogs" onClick={this.NavadvancesettingtoggleparActive}></i>

</div>
<DropdownButton  className="fontsizedrop" id="dropdown-basic-button" title={this.state.NavDesktopfontsizeheaderButtonActive}>
  {
  array1.map((item,i)=>
  {
    return(
      <Dropdown.Item   key={i} onClick={()=>this.NavheaderfontsizebuttonActive(item)}>{item} </Dropdown.Item>
    )
  })
}
</DropdownButton>

<div>
  <p className="bckseptshow">Color</p>
  <i class="fas fa-palette" onClick={this.NavTextcolorsettingopenshowbuttonActive}></i>
</div>

<div>
  <p className="bckseptshow"> Background</p>
<i class="fas fa-fill-drip " onClick={this.NavcolorsettingopenshowbuttonActive}></i>
</div>

{
  this.state.NavopencolorsettingshowbuttonActive?
  <SketchPicker
  className="editopoistionpicker"
  color={this.state.NavDesktopHeaderbackgroundshowbuttonActive}
  onChangeComplete={this.NavhandleChangeCompleteshowbuttonActive}
/>
  :null
}
{
  this.state.NavTextopencolorsettingshowbuttonActive?
  <SketchPicker
  className="editopoistionpicker"
  color={this.state.NavTextDesktopHeaderbackgroundshowbuttonActive}
  onChangeComplete={this.NavTexthandleChangeCompleteshowbuttonActive}
/>
  :null
}


<select value={this.state.NavDesktopheadfontweightNamebuttonActive} onChange={this.NavhandleChangebuttonActive} >
            {  
            Fontweight.map(item=>
              {
                return(
                  <option value={item.name}>{item.name}</option>
                )
              })

            }
          </select>
</div>
</>
:null
         }
<div></div>
{
  this.state.NavshowtogglesettingActive?
  <>
<div className="advancedsetting">
<i class="fas fa-arrow-left cursorpinterback" onClick={this.NavbacavdancesettingActive}></i>
<h1 className="dvancestting">Advanced Setting </h1>


<div className="labeltextalignmentdiv">
<h1 className="adavncesetiontitle">Text change</h1>

</div>


<div className="advancesetting">
  <div className="divpaddingsection divpaddingsectioncustim">
  <input type="text" name="chatnav"  value={this.state.chatnav} onChange={this.inputValuestying}/>
  </div>
  </div>
  <div className="labeltextalignmentdiv">
<h1 className="adavncesetiontitle">Url change</h1>

</div>


<div className="advancesetting">
  <div className="divpaddingsection divpaddingsectioncustim">
  <input type="text" name="Urlchange"  value={this.state.Urlchange} onChange={this.inputValuestying}/>
  </div>
  </div>

<div className="labeltextalignmentdiv">
<h1 className="adavncesetiontitle">Width</h1>
<label className="labelpx">px</label>
</div>

<div className="advancesetting">
  <div className="divpaddingsection">
  <input type="number" name="NavnextbuttonwidthActive" min="0" value={this.state.NavnextbuttonwidthActive} onChange={this.inputValuestying}/>
 
  </div>
  </div>
  <div className="labeltextalignmentdiv">
<h1 className="adavncesetiontitle">Height</h1>
<label className="labelpx">px</label>
</div>


<div className="advancesetting">
  <div className="divpaddingsection ">
  <input type="number" name="NavnextbuttonheightActive" min="0" value={this.state.NavnextbuttonheightActive} onChange={this.inputValuestying}/>
  </div>
  </div>
  <div className="labeltextalignmentdiv">
<h1 className="adavncesetiontitle">Border</h1>
<label className="labelpx">px</label>
</div>

<div className="advancesetting">
  <div className="divpaddingsection bodercustomnextbuttton">
  <input type="number" name="NavnextbuttonborderAcitve" min="0" value={this.state.NavnextbuttonborderAcitve} onChange={this.inputValuestying}/>
  <p className="showsolid">solid</p>
  <input type="text" onClick={this.NavcolorbordershowfunActive} value={this.state.NavDesktopHeaderborderbuttonActive}/>
  </div>
  </div>
  {
  this.state.NavshowsickerpickercolorActive?
  <SketchPicker
  className="editopoistionpicker"
  color={this.state.NavDesktopHeaderborderbuttonActive}
  onChangeComplete={this.NavhandleChangeCompleteforborderActive}
/>
  :null
}


<div className="labeltextalignmentdiv">
<h1 className="adavncesetiontitle">padding</h1>
<label className="labelpx">px</label>
</div>
<div className="advancesetting">
  <div className="divpaddingsection">
  <input type="number" name="NavDNextpaddingLeftActive" min="0" value={this.state.NavDNextpaddingLeftActive} onChange={this.inputValuestying}/>
  <label className="labeltexting">LEFT</label>
  </div>

  <div className="divpaddingsection">
 <input type="number" name="NavDNextpaddingRightActive" min="0" value={this.state.NavDNextpaddingRightActive} onChange={this.inputValuestying}/>
  <label className="labeltexting">RIGHT</label>
  </div>

   <div className="divpaddingsection">
  <input type="number" name="NavDNextpaddingTopActive" min="0" value={this.state.NavDNextpaddingTopActive} onChange={this.inputValuestying}/>
  <label  className="labeltexting">Top</label>
  </div>
   <div className="divpaddingsection">
  <input type="number" name="NavDNextpaddingBottomActive" min="0" value={this.state.NavDNextpaddingBottomActive} onChange={this.inputValuestying}/>
  <label className="labeltexting">Bottom</label>
  </div>

</div>


<div className="labeltextalignmentdiv">
<h1 className="adavncesetiontitle">Margin</h1>
<label className="labelpx">px</label>
</div>
<div className="advancesetting">
  <div className="divpaddingsection">
  <input type="number" name="NavDButtonLeftActive" min="0" value={this.state.NavDButtonLeftActive} onChange={this.inputValuestying}/>
  <label className="labeltexting">LEFT</label>
  </div>

  <div className="divpaddingsection">
 <input type="number" name="NavDButtonRightActive" min="0" value={this.state.NavDButtonRightActive} onChange={this.inputValuestying}/>
  <label className="labeltexting">RIGHT</label>
  </div>

   <div className="divpaddingsection">
  <input type="number" name="NavDButtonTopActive" min="0" value={this.state.NavDButtonTopActive} onChange={this.inputValuestying}/>
  <label  className="labeltexting">Top</label>
  </div>
   <div className="divpaddingsection">
  <input type="number" name="NavDButtonBottomActive" min="0" value={this.state.NavDButtonBottomActive} onChange={this.inputValuestying}/>
  <label className="labeltexting">Bottom</label>
  </div>

</div>
<div className="labeltextalignmentdiv">
<h1 className="adavncesetiontitle">Alignment Text</h1>
<label className="labelpx">px</label>
</div>

<div className="advancesetting">
<i onClick={()=>this.NavDesktopparagraphalignmentfunctionnextActive("right")}  class="fas fa-align-right alignmentheadericon"></i>
<i onClick={()=>this.NavDesktopparagraphalignmentfunctionnextActive("center")}  class="fas fa-align-justify alignmentheadericon"></i>
<i onClick={()=>this.NavDesktopparagraphalignmentfunctionnextActive("left")}  class="fas fa-align-left alignmentheadericon"></i>
</div>

</div>
</>
:null
        }

</div>
</Draggable >

</ClickAwayListener>
</>
:null
        }


  <div>
    <div className="navbarshowing"  >
      <img style={{width:`${this.state.navbuttonwidth}px`,height:`${this.state.navbuttonheight}px`}} src={this.state.imagePreviewUrl} alt=""/>
      <label   style={destopkbuttonActiveNavbar} >
      {this.state.chatnav}
      </label>
    </div>
{/* {
  this.state.showeditor?
<div className="editsection">
      <label className="editorbacktext" onClick={this.imagesettingshow}  >Edit</label>
    </div>
  :null
} */}
    
  </div>
<div className="row m-0 togglerow " style={{width:this.state.mobilewidth,background:this.state.DesktopHeaderborderbuttonbackgrond}} onMouseOver={()=>this.setState({
      showeditor:false
    })}>
{
     this.state.showmediumsettingshow?
     <>
<ClickAwayListener onClickAway={this.setdropshowoutside}> 
 <Draggable bounds="parent" defaultPosition={{x: 0, y: 0}} {...dragHandlers} >
             
           
 <div className="d-flex">

   {
     this.state.showmediumsetting?
     <>
  

<div className="richtextbox">
  <div className="avdancesettinsetion">
<i class="fas fa-cogs settingcussor" onClick={this.advancesettingtoggle}></i>
</div>
<DropdownButton  className="fontsizedrop" id="dropdown-basic-button" title={this.state.Desktopfontsizeheader}>
  {
  array1.map((item,i)=>
  {
    return(
      <Dropdown.Item   key={i} onClick={()=>this.headerfontsize(item)}>{item} </Dropdown.Item>
    )
  })
}
</DropdownButton>
<i class="fas fa-palette " onClick={this.colorsettingopen}></i>
{
  this.state.opencolorsetting?
  <SketchPicker
  className="editopoistionpicker"
  color={this.state.DesktopHeaderbackground}
  onChangeComplete={this.handleChangeComplete}
/>
  :null
}
<select value={this.state.DesktopheadfontweightName} onChange={this.handleChange} >
            {  
            Fontweight.map(item=>
              {
                return(
                  <option value={item.name}>{item.name}</option>
                )
              })

            }
          </select>
</div>
</>
:null
        }
<div></div>
{
  this.state.showtogglesetting?
  <>
<div className="advancedsetting">
<i class="fas fa-arrow-left cursorpinterback" onClick={this.bacavdancesetting}></i>
<h1 className="dvancestting">Advanced Setting</h1>
<div className="labeltextalignmentdiv">
<h1 className="adavncesetiontitle">padding</h1>
<label className="labelpx">px</label>
</div>
<div className="advancesetting">
  <div className="divpaddingsection">
  <input type="number" name="DHpaddingLeft" min="0" value={this.state.DHpaddingLeft} onChange={this.inputValuestying}/>
  <label className="labeltexting">LEFT</label>
  </div>

  <div className="divpaddingsection">
 <input type="number" name="DHpaddingRight" min="0" value={this.state.DHpaddingRight} onChange={this.inputValuestying}/>
  <label className="labeltexting">RIGHT</label>
  </div>

   <div className="divpaddingsection">
  <input type="number" name="DHpaddingTop" min="0" value={this.state.DHpaddingTop} onChange={this.inputValuestying}/>
  <label  className="labeltexting">Top</label>
  </div>
   <div className="divpaddingsection">
  <input type="number" name="DHpaddingBottom" min="0" value={this.state.DHpaddingBottom} onChange={this.inputValuestying}/>
  <label className="labeltexting">Bottom</label>
  </div>

</div>


<div className="labeltextalignmentdiv">
<h1 className="adavncesetiontitle">Margin</h1>
<label className="labelpx">px</label>
</div>
<div className="advancesetting">
  <div className="divpaddingsection">
  <input type="number" name="DHMLeft" min="0" value={this.state.DHMLeft} onChange={this.inputValuestying}/>
  <label className="labeltexting">LEFT</label>
  </div>

  <div className="divpaddingsection">
 <input type="number" name="DHMRight" min="0" value={this.state.DHMRight} onChange={this.inputValuestying}/>
  <label className="labeltexting">RIGHT</label>
  </div>

   <div className="divpaddingsection">
  <input type="number" name="DHMTop" min="0" value={this.state.DHMTop} onChange={this.inputValuestying}/>
  <label  className="labeltexting">Top</label>
  </div>
   <div className="divpaddingsection">
  <input type="number" name="DHMBottom" min="0" value={this.state.DHMBottom} onChange={this.inputValuestying}/>
  <label className="labeltexting">Bottom</label>
  </div>

</div>
<div className="labeltextalignmentdiv">
<h1 className="adavncesetiontitle">Alignment Text</h1>
<label className="labelpx">px</label>
</div>

<div className="advancesetting">
<i onClick={()=>this.Desktopheaderalignmentfunction("right")}  class="fas fa-align-right alignmentheadericon"></i>
<i onClick={()=>this.Desktopheaderalignmentfunction("center")}  class="fas fa-align-justify alignmentheadericon"></i>
<i onClick={()=>this.Desktopheaderalignmentfunction("left")}  class="fas fa-align-left alignmentheadericon"></i>
</div>

</div>
</>
:null
        }

</div>
</Draggable >

</ClickAwayListener>
</>
:null
        }
{/* scond */}







{
     this.state.showmediumsettingshowpar?
     <>
<ClickAwayListener onClickAway={this.setdropshowoutsidepar}> 
 <Draggable bounds="parent" defaultPosition={{x: 0, y: 0}} {...dragHandlers} >
             
           
 <div className="d-flex">

   {
     this.state.showmediumsettingpar?
     <>
  

<div className="richtextbox">
  <div className="avdancesettinsetion">
<i class="fas fa-cogs" onClick={this.advancesettingtogglepar}></i>
</div>
<DropdownButton  className="fontsizedrop" id="dropdown-basic-button" title={this.state.Desktopfontsizeheaderpar}>
  {
  array1.map((item,i)=>
  {
    return(
      <Dropdown.Item   key={i} onClick={()=>this.headerfontsizepar(item)}>{item} </Dropdown.Item>
    )
  })
}
</DropdownButton>
<i class="fas fa-palette" onClick={this.colorsettingopenpar}></i>

{
  this.state.opencolorsettingpar?
  <SketchPicker
  className="editopoistionpicker"
  color={this.state.DesktopHeaderbackgroundpar}
  onChangeComplete={this.handleChangeCompletepar}
/>
  :null
}
<select value={this.state.DesktopheadfontweightNamepar} onChange={this.handleChangepar} >
            {  
            Fontweight.map(item=>
              {
                return(
                  <option value={item.name}>{item.name}</option>
                )
              })

            }
          </select>
</div>
</>
:null
        }
<div></div>
{
  this.state.showtogglesettingpar?
  <>
<div className="advancedsetting">
<i class="fas fa-arrow-left cursorpinterback" onClick={this.bacavdancesettingpar}></i>
<h1 className="dvancestting">Advanced Setting 2</h1>
<div className="labeltextalignmentdiv">
<h1 className="adavncesetiontitle">padding</h1>
<label className="labelpx">px</label>
</div>
<div className="advancesetting">
  <div className="divpaddingsection">
  <input type="number" name="DHMpaddingLeft" min="0" value={this.state.DHMpaddingLeft} onChange={this.inputValuestying}/>
  <label className="labeltexting">LEFT</label>
  </div>

  <div className="divpaddingsection">
 <input type="number" name="DHMpaddingRight" min="0" value={this.state.DHMpaddingRight} onChange={this.inputValuestying}/>
  <label className="labeltexting">RIGHT</label>
  </div>

   <div className="divpaddingsection">
  <input type="number" name="DHMpaddingTop" min="0" value={this.state.DHMpaddingTop} onChange={this.inputValuestying}/>
  <label  className="labeltexting">Top</label>
  </div>
   <div className="divpaddingsection">
  <input type="number" name="DHMpaddingBottom" min="0" value={this.state.DHMpaddingBottom} onChange={this.inputValuestying}/>
  <label className="labeltexting">Bottom</label>
  </div>

</div>


<div className="labeltextalignmentdiv">
<h1 className="adavncesetiontitle">Margin</h1>
<label className="labelpx">px</label>
</div>
<div className="advancesetting">
  <div className="divpaddingsection">
  <input type="number" name="DHPLeft" min="0" value={this.state.DHPLeft} onChange={this.inputValuestying}/>
  <label className="labeltexting">LEFT</label>
  </div>

  <div className="divpaddingsection">
 <input type="number" name="DHPRight" min="0" value={this.state.DHPRight} onChange={this.inputValuestying}/>
  <label className="labeltexting">RIGHT</label>
  </div>

   <div className="divpaddingsection">
  <input type="number" name="DHPTop" min="0" value={this.state.DHPTop} onChange={this.inputValuestying}/>
  <label  className="labeltexting">Top</label>
  </div>
   <div className="divpaddingsection">
  <input type="number" name="DHPBottom" min="0" value={this.state.DHPBottom} onChange={this.inputValuestying}/>
  <label className="labeltexting">Bottom</label>
  </div>

</div>
<div className="labeltextalignmentdiv">
<h1 className="adavncesetiontitle">Alignment Text</h1>
<label className="labelpx">px</label>
</div>

<div className="advancesetting">
<i onClick={()=>this.Desktopparagraphalignmentfunction("right")}  class="fas fa-align-right alignmentheadericon"></i>
<i onClick={()=>this.Desktopparagraphalignmentfunction("center")}  class="fas fa-align-justify alignmentheadericon"></i>
<i onClick={()=>this.Desktopparagraphalignmentfunction("left")}  class="fas fa-align-left alignmentheadericon"></i>
</div>

</div>
</>
:null
        }

</div>
</Draggable >

</ClickAwayListener>
</>
:null
        }

{/* next */}








{
     this.state.shownxtbutton?
     <>
<ClickAwayListener onClickAway={this.closenoactive}  > 
 <Draggable bounds="parent" defaultPosition={{x: 0, y: 0}} {...dragHandlers} >
             
           
 <div className="d-flex">

    {
     this.state.showbuttonsettingrich? 
     <>
  

<div className="richtextbox">
  <div className="avdancesettinsetion">
<i class="fas fa-cogs settingcussor" onClick={this.advancesettingtogglenext}></i>

</div>
<DropdownButton  className="fontsizedrop" id="dropdown-basic-button" title={this.state.DesktopfontsizeheaderButton}>
  {
  array1.map((item,i)=>
  {
    return(
      <Dropdown.Item   key={i} onClick={()=>this.headerfontsizebutton(item)}>{item} </Dropdown.Item>
    )
  })
}
</DropdownButton>

<div>
  <p className="bckseptshow">Color</p>
  <i class="fas fa-palette" onClick={this.Textcolorsettingopenshowbutton}></i>
</div>

<div>
  <p className="bckseptshow"> Background</p>
<i class="fas fa-fill-drip " onClick={this.colorsettingopenshowbutton}></i>
</div>

{
  this.state.opencolorsettingshowbutton?
  <SketchPicker
  className="editopoistionpicker"
  color={this.state.DesktopHeaderbackgroundshowbutton}
  onChangeComplete={this.handleChangeCompleteshowbutton}
/>
  :null
}
{
  this.state.Textopencolorsettingshowbutton?
  <SketchPicker
  className="editopoistionpicker"
  color={this.state.TextDesktopHeaderbackgroundshowbutton}
  onChangeComplete={this.TexthandleChangeCompleteshowbutton}
/>
  :null
}


<select value={this.state.DesktopheadfontweightNamebutton} onChange={this.handleChangebutton} >
            {  
            Fontweight.map(item=>
              {
                return(
                  <option value={item.name}>{item.name}</option>
                )
              })

            }
          </select>
</div>
</>
:null
         }
<div></div>
{
  this.state.showtogglesettingpadvnceasetting?
  <>
<div className="advancedsetting">
<i class="fas fa-arrow-left cursorpinterback" onClick={this.bacavdancesettingnext}></i>
<h1 className="dvancestting">Advanced Setting 2</h1>


<div className="labeltextalignmentdiv">
<h1 className="adavncesetiontitle">Text Change</h1>

</div>


<div className="advancesetting">
  <div className="divpaddingsection " style={{width:"100%"}}>
  <input type="text" name="ButtonTextnextnoActive" min="0" value={this.state.ButtonTextnextnoActive} onChange={this.inputValuestying}/>
  </div>
  </div>
<div className="labeltextalignmentdiv">
<h1 className="adavncesetiontitle">Width</h1>
<label className="labelpx">px</label>
</div>

<div className="advancesetting">
  <div className="divpaddingsection">
  <input type="number" name="nextbuttonwidth" min="0" value={this.state.nextbuttonwidth} onChange={this.inputValuestying}/>
 
  </div>
  </div>
  <div className="labeltextalignmentdiv">
<h1 className="adavncesetiontitle">Height</h1>
<label className="labelpx">px</label>
</div>


<div className="advancesetting">
  <div className="divpaddingsection ">
  <input type="number" name="nextbuttonheight" min="0" value={this.state.nextbuttonheight} onChange={this.inputValuestying}/>
  </div>
  </div>
  <div className="labeltextalignmentdiv">
<h1 className="adavncesetiontitle">Border</h1>
<label className="labelpx">px</label>
</div>

<div className="advancesetting">
  <div className="divpaddingsection bodercustomnextbuttton">
  <input type="number" name="nextbuttonborder" min="0" value={this.state.nextbuttonborder} onChange={this.inputValuestying}/>
  <p className="showsolid">solid</p>
  <input type="text" onClick={this.colorbordershowfun} value={this.state.DesktopHeaderborderbutton}/>
  </div>
  </div>
  {
  this.state.showsickerpickercolor?
  <SketchPicker
  className="editopoistionpicker"
  color={this.state.DesktopHeaderborderbutton}
  onChangeComplete={this.handleChangeCompleteforborder}
/>
  :null
}


<div className="labeltextalignmentdiv">
<h1 className="adavncesetiontitle">padding</h1>
<label className="labelpx">px</label>
</div>
<div className="advancesetting">
  <div className="divpaddingsection">
  <input type="number" name="DNextpaddingLeft" min="0" value={this.state.DNextpaddingLeft} onChange={this.inputValuestying}/>
  <label className="labeltexting">LEFT</label>
  </div>

  <div className="divpaddingsection">
 <input type="number" name="DNextpaddingRight" min="0" value={this.state.DNextpaddingRight} onChange={this.inputValuestying}/>
  <label className="labeltexting">RIGHT</label>
  </div>

   <div className="divpaddingsection">
  <input type="number" name="DNextpaddingTop" min="0" value={this.state.DNextpaddingTop} onChange={this.inputValuestying}/>
  <label  className="labeltexting">Top</label>
  </div>
   <div className="divpaddingsection">
  <input type="number" name="DNextpaddingBottom" min="0" value={this.state.DNextpaddingBottom} onChange={this.inputValuestying}/>
  <label className="labeltexting">Bottom</label>
  </div>

</div>


<div className="labeltextalignmentdiv">
<h1 className="adavncesetiontitle">Margin</h1>
<label className="labelpx">px</label>
</div>
<div className="advancesetting">
  <div className="divpaddingsection">
  <input type="number" name="DButtonLeft" min="0" value={this.state.DButtonLeft} onChange={this.inputValuestying}/>
  <label className="labeltexting">LEFT</label>
  </div>

  <div className="divpaddingsection">
 <input type="number" name="DButtonRight" min="0" value={this.state.DButtonRight} onChange={this.inputValuestying}/>
  <label className="labeltexting">RIGHT</label>
  </div>

   <div className="divpaddingsection">
  <input type="number" name="DButtonTop" min="0" value={this.state.DButtonTop} onChange={this.inputValuestying}/>
  <label  className="labeltexting">Top</label>
  </div>
   <div className="divpaddingsection">
  <input type="number" name="DButtonBottom" min="0" value={this.state.DButtonBottom} onChange={this.inputValuestying}/>
  <label className="labeltexting">Bottom</label>
  </div>

</div>
<div className="labeltextalignmentdiv">
<h1 className="adavncesetiontitle">Alignment Text</h1>
<label className="labelpx">px</label>
</div>

<div className="advancesetting">
<i onClick={()=>this.Desktopparagraphalignmentfunctionnext("right")}  class="fas fa-align-right alignmentheadericon"></i>
<i onClick={()=>this.Desktopparagraphalignmentfunctionnext("center")}  class="fas fa-align-justify alignmentheadericon"></i>
<i onClick={()=>this.Desktopparagraphalignmentfunctionnext("left")}  class="fas fa-align-left alignmentheadericon"></i>
</div>

</div>
</>
:null
        }

</div>
</Draggable >

</ClickAwayListener>
</>
:null
        }







{/* button on active */}



{
     this.state.showntbuttonactive?
     <>
<ClickAwayListener onClickAway={this.closeactivebuttonstate} > 
 <Draggable bounds="parent" defaultPosition={{x: 0, y: 0}} {...dragHandlers} >
             
           
 <div className="d-flex">

    {
     this.state.showmediumsettingActive? 
     <>
  

<div className="richtextbox">
  <div className="avdancesettinsetion">
<i class="fas fa-cogs" onClick={this.advancesettingtoggleparActive}></i>

</div>
<DropdownButton  className="fontsizedrop" id="dropdown-basic-button" title={this.state.DesktopfontsizeheaderButtonActive}>
  {
  array1.map((item,i)=>
  {
    return(
      <Dropdown.Item   key={i} onClick={()=>this.headerfontsizebuttonActive(item)}>{item} </Dropdown.Item>
    )
  })
}
</DropdownButton>

<div>
  <p className="bckseptshow">Color</p>
  <i class="fas fa-palette" onClick={this.TextcolorsettingopenshowbuttonActive}></i>
</div>

<div>
  <p className="bckseptshow"> Background</p>
<i class="fas fa-fill-drip " onClick={this.colorsettingopenshowbuttonActive}></i>
</div>

{
  this.state.opencolorsettingshowbuttonActive?
  <SketchPicker
  className="editopoistionpicker"
  color={this.state.DesktopHeaderbackgroundshowbuttonActive}
  onChangeComplete={this.handleChangeCompleteshowbuttonActive}
/>
  :null
}
{
  this.state.TextopencolorsettingshowbuttonActive?
  <SketchPicker
  className="editopoistionpicker"
  color={this.state.TextDesktopHeaderbackgroundshowbuttonActive}
  onChangeComplete={this.TexthandleChangeCompleteshowbuttonActive}
/>
  :null
}


<select value={this.state.DesktopheadfontweightNamebuttonActive} onChange={this.handleChangebuttonActive} >
            {  
            Fontweight.map(item=>
              {
                return(
                  <option value={item.name}>{item.name}</option>
                )
              })

            }
          </select>
</div>
</>
:null
         }
<div></div>
{
  this.state.showtogglesettingActive?
  <>
<div className="advancedsetting">
<i class="fas fa-arrow-left cursorpinterback" onClick={this.bacavdancesettingActive}></i>
<h1 className="dvancestting">Advanced Setting 2</h1>


<div className="labeltextalignmentdiv">
<h1 className="adavncesetiontitle">Text Change</h1>

</div>


<div className="advancesetting">
  <div className="divpaddingsection " style={{width:"100%"}}>
  <input type="text" name="ButtonTextnext" min="0" value={this.state.ButtonTextnext} onChange={this.inputValuestying}/>
  </div>
  </div>



<div className="labeltextalignmentdiv">



<h1 className="adavncesetiontitle">Width</h1>
<label className="labelpx">px</label>
</div>

<div className="advancesetting">
  <div className="divpaddingsection">
  <input type="number" name="nextbuttonwidthActive" min="0" value={this.state.nextbuttonwidthActive} onChange={this.inputValuestying}/>
 
  </div>
  </div>
  <div className="labeltextalignmentdiv">
<h1 className="adavncesetiontitle">Height</h1>
<label className="labelpx">px</label>
</div>


<div className="advancesetting">
  <div className="divpaddingsection ">
  <input type="number" name="nextbuttonheightActive" min="0" value={this.state.nextbuttonheightActive} onChange={this.inputValuestying}/>
  </div>
  </div>
  <div className="labeltextalignmentdiv">
<h1 className="adavncesetiontitle">Border</h1>
<label className="labelpx">px</label>
</div>

<div className="advancesetting">
  <div className="divpaddingsection bodercustomnextbuttton">
  <input type="number" name="nextbuttonborderAcitve" min="0" value={this.state.nextbuttonborderAcitve} onChange={this.inputValuestying}/>
  <p className="showsolid">solid</p>
  <input type="text" onClick={this.colorbordershowfunActive} value={this.state.DesktopHeaderborderbuttonActive}/>
  </div>
  </div>
  {
  this.state.showsickerpickercolorActive?
  <SketchPicker
  className="editopoistionpicker"
  color={this.state.DesktopHeaderborderbuttonActive}
  onChangeComplete={this.handleChangeCompleteforborderActive}
/>
  :null
}


<div className="labeltextalignmentdiv">
<h1 className="adavncesetiontitle">padding</h1>
<label className="labelpx">px</label>
</div>
<div className="advancesetting">
  <div className="divpaddingsection">
  <input type="number" name="DNextpaddingLeftActive" min="0" value={this.state.DNextpaddingLeftActive} onChange={this.inputValuestying}/>
  <label className="labeltexting">LEFT</label>
  </div>

  <div className="divpaddingsection">
 <input type="number" name="DNextpaddingRightActive" min="0" value={this.state.DNextpaddingRightActive} onChange={this.inputValuestying}/>
  <label className="labeltexting">RIGHT</label>
  </div>

   <div className="divpaddingsection">
  <input type="number" name="DNextpaddingTopActive" min="0" value={this.state.DNextpaddingTopActive} onChange={this.inputValuestying}/>
  <label  className="labeltexting">Top</label>
  </div>
   <div className="divpaddingsection">
  <input type="number" name="DNextpaddingBottomActive" min="0" value={this.state.DNextpaddingBottomActive} onChange={this.inputValuestying}/>
  <label className="labeltexting">Bottom</label>
  </div>

</div>


<div className="labeltextalignmentdiv">
<h1 className="adavncesetiontitle">Margin</h1>
<label className="labelpx">px</label>
</div>
<div className="advancesetting">
  <div className="divpaddingsection">
  <input type="number" name="DButtonLeftActive" min="0" value={this.state.DButtonLeftActive} onChange={this.inputValuestying}/>
  <label className="labeltexting">LEFT</label>
  </div>

  <div className="divpaddingsection">
 <input type="number" name="DButtonRightActive" min="0" value={this.state.DButtonRightActive} onChange={this.inputValuestying}/>
  <label className="labeltexting">RIGHT</label>
  </div>

   <div className="divpaddingsection">
  <input type="number" name="DButtonTopActive" min="0" value={this.state.DButtonTopActive} onChange={this.inputValuestying}/>
  <label  className="labeltexting">Top</label>
  </div>
   <div className="divpaddingsection">
  <input type="number" name="DButtonBottomActive" min="0" value={this.state.DButtonBottomActive} onChange={this.inputValuestying}/>
  <label className="labeltexting">Bottom</label>
  </div>

</div>
<div className="labeltextalignmentdiv">
<h1 className="adavncesetiontitle">Alignment Text</h1>
<label className="labelpx">px</label>
</div>

<div className="advancesetting">
<i onClick={()=>this.DesktopparagraphalignmentfunctionnextActive("right")}  class="fas fa-align-right alignmentheadericon"></i>
<i onClick={()=>this.DesktopparagraphalignmentfunctionnextActive("center")}  class="fas fa-align-justify alignmentheadericon"></i>
<i onClick={()=>this.DesktopparagraphalignmentfunctionnextActive("left")}  class="fas fa-align-left alignmentheadericon"></i>
</div>

</div>
</>
:null
        }

</div>
</Draggable >

</ClickAwayListener>
</>
:null
        }


     {/* header */}


{/* end of header */}









{/* next */}
<div className="col-md-6 leftsidesection">


{
  this.state.showclassactive?
<Draggable bounds="parent" defaultPosition={{x: 0, y: 0}} {...dragHandlers} >
<div className="showthetoggleactivandinactive">
  <p onClick={this.activenextbutton} className="m-0"style={{cursor:"pointer"}}>no active</p>
  <p onClick={this.noactivenextbutton} className="m-0"style={{cursor:"pointer"}}> active</p>
</div>
</Draggable>
  :null
}


<div>
<div style={destopheadertitleDIv}>
{/* <span contenteditable="true"  name="headertitleinput" style={destopheadertitle} onClick={this.setdropshowoutsideTrue}
  onInput={e => this.inputValuestyingtyling(e)} >
{this.state.headertitleinput}
</span> */}
<span style={destopheadertitle}>{this.state.headertitleinput}</span>
</div>





<div style={destopheadertitleDIvpar}>
{/* <span contenteditable="true"  name="headertitleinputpar" style={destopheadertitlepar}  onClick={this.setdropshowoutsideTruepar}
onInput={e => this.inputValuestyingtyling1(e)}>
} */}
<span style={destopheadertitlepar} >{this.state.headertitleinputpar}</span>

</div>
  

<label htmlFor="" style={this.state.active?destopkbutton:destopkbuttonActive} >
  {this.state.active?this.state.ButtonTextnextnoActive:this.state.ButtonTextnext}
</label>

</div>
  </div>
  
  
  
  
  
  

  
  
  
  <div className="col-md-6 popoajadnj">

 

 
<div className="" style={{position:"relative",display:"flex",justifyContent:"center"}} >
  <ReactPlayer url={this.state.selectedFile}
         className="player_video"
         playing={false}
         controls={controls}
        
         loop={false}
         volume={volume}
         muted={false}
         onReady={() => console.log('onReady')}
         onStart={onstartfun}
         onPlay={handlePlay}
         onPause={handlePause}
         onBuffer={OnBuffer}
         onSeek={OnseekFun}
         onEnded={this.handleEnded}
         onProgress={handleProgress}
          />
          <div>
            
          </div>


          {/* {
  this.state.showeditor1?
<div className="editsection1">
      <label className="editorbacktext" onClick={this.imagesettingshowfu}  >Edit</label>
    </div>
  :null
} */}
</div>


{
  this.state.imagesettinghsow1?
  <Draggable bounds="parent" defaultPosition={{x: 0, y: 0}} {...dragHandlers} >

  <div className="navbardraaging">
    <h1 className="dvancestting">Video Setting</h1>
 
   <div class="upload-btn-wrapper">
   <button class="btn-vsa">Please Upload Your Video</button>
   <input type="file" name="myfile" onChange={(e)=>this.onChangeHandler(e)} />
   </div>
  </div>
</Draggable>
//   <Draggable bounds="parent" defaultPosition={{x: 0, y: 0}} {...dragHandlers} >

// </Draggable>
:null
}

{
  this.state.showeditorback?
  <Draggable bounds="parent" defaultPosition={{x: 0, y: 0}} {...dragHandlers} >

  <div className="navbardraaging">
    <h1 className="dvancestting">Background Setting</h1>
    <SketchPicker
  className="editopoistionpicker"
  color={this.state.DesktopHeaderborderbuttonbackgrond}
  onChangeComplete={this.NavhandleChangeCompleteforborderActivebackgrond}
/>
  </div>
</Draggable>
//   <Draggable bounds="parent" defaultPosition={{x: 0, y: 0}} {...dragHandlers} >

// </Draggable>
:null
}
{/* <button className="sasvebuuto bbvcvc" onClick={this.backsetting} ><i class="fas fa-fill-drip"></i></button> */}
{/* <form onSubmit={this.}>
                    <label>
                        Upload a file: <br /><br />
                        <input type="file" name="file" onChange={this.onChangeHandler} />
                    </label>
                    <br /><br />
                    <button type="submit">
                        Upload
                    </button>
                </form > */}

                {/* <button className="sasvebuuton" onClick={this.postcustomdata}>save</button> */}
          
</div>
</div>
 <div className="">
            <div className="">
                <div className="">
                <div className="">
                {/* <h1 className="head1">Set Yourself Up For</h1>
 <h1 className="head2">The Next Bitcoin Bull Run</h1> */}

 <div className="" >
   <>
   {/* <YouTube
        videoId='3cbd0-nyc3b-nyc-zg02'
        opts={opts}
     
        onPause={VideoonPause} 
     
        onPlay={VideoOnPlay}  

      /> 
       */}
      

< div >
                
            </div >

 </>
 </div>


 {/* <p className="para">The Greatest Opprtunity Of Your Lifetime Is Underway!</p> */}



{/* 

 <Button onClick={handleShow} className="popupbutton">
            GET ACCESS TO FULL WEBINAR
      </Button> */}
            </div>
 
            </div>
            </div>



          

      <Modal show={show} onHide={handleClose} enforceFocus={false}  backdrop= 'static'
    keyboard={false}>
        <Modal.Header closeButton>
          <Modal.Title>Enter Your Details
</Modal.Title>
        </Modal.Header>
        <Modal.Body>


        <Form>

        <Form.Group controlId="formBasicname">
    <Form.Label>Name</Form.Label>
    <Form.Control type="text"  name="name"  placeholder="Enter Your Name" onFocus={NameFunction}  onChange={inputValue}  />

  </Form.Group>
  <Form.Group controlId="formBasicEmail">
    <Form.Label>Email address</Form.Label>
    <Form.Control type="email" name="email"  placeholder="name@email.com"  onChange={inputValue}  required />
   
  </Form.Group>



</Form>
        </Modal.Body>
        <Modal.Footer>
       {next?
    <NavLink to="/Fp2"   >  <Button  className="next" onClick={Emailsubmitedata} >
     Next
          </Button></NavLink> 
        :""}
        </Modal.Footer>
      </Modal>
      
      </div>
            </>
               
          );
        }}
      </ProductConsumer>
    );
  }
}



